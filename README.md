# PA_annotations

Infos générales
---------------

- **Etudiant** : [Anthony Gugler] - anthony.gugler@edu.hefr.ch
- **Professeur** : [Carlos Pena] - carlos.pena@heig-vd.ch
- **Adjoint** : [Xavier Brochet] - xavier.brochet@hes-so.ch
- **Dates** : du 20.02.2023 au 02.06.2023

Contexte 
----------
Dans le cadre de mon Master en science des données à l’HES-Master Lausanne, je réalise un projet d’approfondissement lors de ce deuxième semestre. Ce travail est nécessaire pour préparer la réalisation du travail de master. Nous disposons de trois périodes de cours par semaines dédiées à l’avancée de notre projet. Environ 180 heures de travail seront nécessaires pour effectuer l’ensemble du projet.

Le projet débute le lundi 20 février 2023 et se terminera le 2 juin de la même année avec une défense de projet.
L’approche de la thématique, la gestion de projet, l’analyse, le développement, la présentation et les rapports seront les éléments principaux de l’évaluation de ce projet. 




Description
--------
Dans le domaine de l’apprentissage automatique, les annotations sont des étiquettes ou des labels associés à des données. Elles permettent d’ajouter des informations aux données pour l’entraînement et ainsi réaliser des modèles supervisés de machine Learning capable de réaliser des tâches spécifiques telles que la classification ou la détection d’objets par exemple.

Selon la tâche et le modèle, les annotations peuvent être de différent type. Pour les annotations d’images, un cadre, qui permet de délimiter l’objet détecté, est dessiné sur l’image et une classe est associée à cet objet. Ce travail d’annotation est généralement réalisé manuellement par des annotateurs humains qui examinent chaque donnée et les marquent avec les informations appropriées.

Les annotations de haute qualité sont nécessaires pour l’élaboration de modèle de machine learning performant. Ainsi, le modèle apprend les bons éléments du set d’entraînement et pourra réaliser sa tâche spécifique de manière précise.

Dans de nombreux projets médicaux, il est nécessaire d’ajouter des annotations à des tests médicaux, tels que les radiographies, les scans, les tests de laboratoire par exemple, pour élaborer des prédictions. Ces annotations qui sont le regroupement des appréciations et des commentaires donnés par des experts requièrent des connaissances de personnel médical hautement qualifié. Toutefois, ce processus d’annotation clinique est chronophage et donc coûteux pour traiter ces données. 

Il existe plusieurs possibilités pour accélérer ces tâches d’annotation avec des outils informatisés allant de la simple boite à outils graphique jusqu’aux aides basées sur l’intelligence artificielle.

Pour ce projet, l’objectif principal est de réaliser un modèle permettant l’aide à l’annotation semi-automatique de données médicales. Le temps à disposition permet seulement la réalisation d’un "proof of concept". 





Contenu
-------

Ce dépôt contient :

- les fichiers python du projet sont dans le dossier `Codes/`.
- la documentation relative au projet dans le dossier `Documents/` 
- lien vers le git : gitlab.forge.hefr.ch

