# Imputed Stage Project

Ce répertoire contient des notebooks qui implémentent différentes étapes du projet "Imputed Stage".

## Notebooks

- `getting_started.ipynb` : Un guide de démarrage rapide pour accéder aux données HIRID.
- `imputed_stage - Active.ipynb` : Ce notebook contient le code pour le traitement des données avec l'apprentissage actif.
- `imputed_stage - annotation - v2.ipynb` : Version mise à jour du notebook pour l'annotation des données.
- `imputed_stage - annotation.ipynb` : Ce notebook est utilisé pour l'annotation des données.
- `imputed_stage - chronologie chunk.ipynb` : Ce notebook traite de la chronologie des chunks de données.
- `imputed_stage - Cluster.ipynb` : Ce notebook implémente des techniques de clustering sur les données.
- `imputed_stage - modele balanced.ipynb` : Ce notebook contient un modèle qui traite des données équilibrées.
- `imputed_stage - Modele.ipynb` : Ce notebook est utilisé pour le développement et la formation des modèles de ML.
- `imputed_stage - split_data.ipynb` : Ce notebook est utilisé pour diviser les données en ensembles de formation et de test.

## Répertoire

- `data` : Ce dossier contient les données utilisées dans les notebooks. (il faut demander l'accès)

