# Classification de l'arythmie sur l'ECG

Ce répertoire contient plusieurs fichiers de scripts et de données utilisés pour le projet de classification des arythmies ECG.

## Scripts

- `clustering 2.ipynb` : Un autre notebook explorant différentes techniques de clustering.
- `clustering-small copy.ipynb` : Ce cahier est une copie de travail du cahier `clustering-small.ipynb`.
- `clustering-small dbscan.ipynb` : Ce notebook explore l'utilisation de l'algorithme DBSCAN pour le clustering.
- `clustering-small-cl-hierarchical.ipynb` : Ce carnet explore l'utilisation des techniques de clustering hiérarchique.
- `clustering-small-dimention.ipynb` : Ce cahier explore la réduction de la dimensionnalité dans le clustering.
- `clustering-small.ipynb` : Un cahier contenant des expériences de base sur le clustering.
- `clustering.ipynb` : Ce cahier explore différentes techniques de clustering.

## Données

- `mitbih_test.csv` : Jeu de test pour la base de données MIT-BIH.
- mitbih_train.csv` : Ensemble d'entraînement pour la base de données MIT-BIH.
- `ptbdb_abnormal.csv` : Base de données PTB Diagnostic ECG contenant des enregistrements anormaux.
- `ptbdb_normal.csv` : Base de données ECG de diagnostic de la PTB contenant des enregistrements normaux.

